import argparse
import os


class AppConfiguration:

    _CONFIGURATION_MAPPING = {
        'port': {
            'command_line_parameter': '--port',
            'backing_environment_variable': 'PORT',
            'description': 'The port to listen on',
            'type': int,
            'required_argument': False,
            'required': True
        },
        'multiplier': {
            'command_line_parameter': '--multiplier',
            'backing_environment_variable': 'MULTIPLIER',
            'description': 'The number to multiply by',
            'type': float,
            'required_argument': False,
            'required': True
        }
    }

    def __init__(self):

        self._configuration = {}

        argument_parser = argparse.ArgumentParser(description='Yet another demo-application.')
        for parameter, settings in AppConfiguration._CONFIGURATION_MAPPING.items():
            argument_parser.add_argument(
                settings["command_line_parameter"],
                required=settings['required_argument'],
                dest=parameter,
                type=settings['type'],
                help="{:s} (fallback variable is {:s})".format(
                    settings['description'],
                    settings['backing_environment_variable']
                ),
                metavar=f'<{parameter}>'
            )

        argument_parser_result = argument_parser.parse_args()

        for parameter, settings in AppConfiguration._CONFIGURATION_MAPPING.items():
            if getattr(argument_parser_result, parameter, None) is None:
                self._configuration[parameter] = os.environ.get(settings['backing_environment_variable'])
            else:
                self._configuration[parameter] = getattr(argument_parser_result, parameter)
            if self._configuration[parameter] is None and settings['required']:
                raise ValueError("Missing configuration parameter: {:s}, go get some --help".format(parameter))

    def get(self, parameter: str):
        if parameter not in AppConfiguration._CONFIGURATION_MAPPING:
            raise ValueError(f"Unknown configuration parameter: {parameter}")
        return self._configuration.get(parameter)
