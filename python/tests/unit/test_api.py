import os

import pytest
import random
import os
import math

from app_factory import AppFactory


MULTIPLIER = os.environ.get('MULTIPLIER', math.pi)


@pytest.fixture()
def client():
    app = AppFactory.produce_app(multiplier=MULTIPLIER)
    return app.test_client()


def test_404(client):
    response = client.get('/')
    assert response.status_code == 404
    response = client.get('/foo')
    assert response.status_code == 404
    response = client.get('/bar')
    assert response.status_code == 404
    response = client.get('/baz')
    assert response.status_code == 404


def test_multiplication(client):
    response = client.get('/0')
    assert response.status_code == 200
    assert response.json == {'endpoint': 0, 'multiplier': MULTIPLIER, 'result': 0}
    response = client.get('/1')
    assert response.status_code == 200
    assert response.json == {'endpoint': 1, 'multiplier': MULTIPLIER, 'result': MULTIPLIER}
    response = client.get('/{}'.format(MULTIPLIER))
    assert response.status_code == 200
    assert response.json == {'endpoint': MULTIPLIER, 'multiplier': MULTIPLIER, 'result': MULTIPLIER * MULTIPLIER}
    for i in range(10):
        endpoint = random.uniform(0.0, 100.0)
        response = client.get('/{}'.format(endpoint))
        assert response.status_code == 200
        assert response.json == {'endpoint': endpoint, 'multiplier': MULTIPLIER, 'result': endpoint * MULTIPLIER}