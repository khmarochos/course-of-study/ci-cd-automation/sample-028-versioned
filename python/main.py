import waitress

from app_configuration import AppConfiguration
from app_factory import AppFactory



if __name__ == '__main__':

    app_configuration = AppConfiguration()
    app_factory = AppFactory()

    flask_app = app_factory.produce_app(multiplier=app_configuration.get('multiplier'))

    waitress.serve(
        flask_app,
        host='0.0.0.0',
        port=app_configuration.get('port')
    )
