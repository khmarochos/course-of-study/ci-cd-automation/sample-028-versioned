from flask import Flask


class AppFactory:

    @staticmethod
    def produce_app(multiplier: int = 1):
        flask_app = Flask(__name__)

        @flask_app.route('/<int:endpoint>')
        @flask_app.route('/<float:endpoint>')
        def multiply(endpoint: float):
            result = endpoint * multiplier
            return {
                'endpoint': endpoint,
                'multiplier': multiplier,
                'result': result,
            }

        return flask_app
